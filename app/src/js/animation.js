function animationSlides(){
    let vw = window.innerWidth;
    let vh = window.innerHeight;

    tlBackgrounds = new TimelineMax({paused:true, repeat:-1});
    tlBackgrounds.set([backgrounds] ,{x:0, y:0});
    tlBackgrounds.set([titles] ,{x:vw, y:vh});
    
      tlBackgrounds.to(backgrounds[0] , 3,    {x:-20, rotation:0.01            , ease: Power0.easeNone}, 0);
      tlBackgrounds.to(backgrounds[0] , 1.2,  {x:-vw*1.5, rotation:0.01        , ease: Power1.easeOut},  3);
    
      tlBackgrounds.to(backgrounds[1] , 4,    {x:-30, rotation:0.01            , ease: Power0.easeNone}, 3.2);
      tlBackgrounds.to(backgrounds[1] , 1,    {x:-vw*1.5, rotation:0.01        , ease: Power1.easeOut},  7.2);
    
      tlBackgrounds.set(backgrounds[0],       {x:vw}, 7);
    
      tlBackgrounds.to(backgrounds[2] , 3.8,  {x:-30, rotation:0.01            , ease: Power0.easeNone}, 7.2);
      tlBackgrounds.to(backgrounds[0] , 1,    {x:0, rotation:0.01              , ease: Power3.easeOut},   11);
    
        
    tlPlayers = new TimelineMax({paused:true, repeat:-1});
    tlPlayers.set([players] ,{x:vw, y:vh});
    
      tlPlayers.to(players[0] ,   1,    {x:0, y:0, rotation:0.01               , ease: Power0.easeNone}, 0);
      tlPlayers.to(players[0] ,   2,    {x:0, y:-20, rotation:0.01             , ease: Power0.easeNone}, 1);
      tlPlayers.to(players[0] ,   1,    {x:-vw*1.5, y:-vh, rotation:0.01       , ease: Power1.easeOut},  3);
    
      tlPlayers.to(players[1] , 0.8,    {x:0, y:0, rotation:0.01               , ease: Back.easeOut.config(.5)}, 3);
      tlPlayers.to(players[1] , 3.4,    {x:20, y:20, rotation:0.01             , ease: Power0.easeNone}, 3.8);
      tlPlayers.to(players[1] , 1.2,    {x:-2400, y:1000, rotation:0.01        , ease: Power2.easeOut},  7.2);
    
      tlPlayers.to(players[2] , 1,      {x:0, y:0 , rotation:0.01              , ease: Power0.easeNone}, 6.5);
      tlPlayers.to(players[2] , 3.3,    {x:-20, y:20 , rotation:0.01           , ease: Power0.easeNone}, 7.5); 
      tlPlayers.to(players[2] , 1.2,    {x:-vw*1.5, y:vh , rotation:0.01       , ease: Power2.easeOut}, 10.8);
              
    
    
    tlLines = new TimelineMax({paused:true, repeat:-1});
    tlLines.set([verticalLine]    , {y:vh/8});
    tlLines.set([horizontalLine]  , {x:vw/10, y:0});

     tlLines.to(verticalLine ,   11,    {y:-vh/4, rotation:0.01    , ease: Power0.easeNone}, 0);
     tlLines.to(horizontalLine , 11,    {x:-vw/2, rotation:0.01    , ease: Power0.easeNone}, 0);
     tlLines.to(verticalLine ,   1,     {y:-vh*2, rotation:0.01    , ease: Power1.easeOut}, 11);
     tlLines.to(horizontalLine , 1,     {x:-vw*3, rotation:0.01    , ease: Power1.easeOut}, 11);
    
    
    tlNumbers = new TimelineMax({paused:true, repeat:-1});
    tlNumbers.set([numbers] ,{x:vw, y:-vh});
    
      tlNumbers.to(numbers[0] ,   1,  {x:0, y:0, rotation:0.01           , ease: Power0.easeNone},    0);
      tlNumbers.to(numbers[0] ,   6,  {x:0, y:-20, rotation:0.01         , ease: Power0.easeNone},    1);
      tlNumbers.to(numbers[0] ,   1,  {x:-vw*3, y:-vh*2, rotation:0.01   , ease: Power0.easeNone},    7);
    
      tlNumbers.to(numbers[1] , 1,    {x:0, y:0, rotation:0.01           , ease: Power0.easeNone},  6.4);
      tlNumbers.to(numbers[1] , 3.5,  {x:0, y:-20, rotation:0.01         , ease: Power0.easeNone},  7.4);
      tlNumbers.to(numbers[1] , 1.1,  {x:-vw*3, y:vh*2, rotation:0.01    , ease: Power0.easeNone}, 10.9); 
    
       
    tlTitles = new TimelineMax({paused:true, repeat:-1});
    let text1 = titles[1].querySelector('.text-1');
    let text2 = titles[1].querySelector('.text-2');
    let text11 = titles[2].querySelector('.text-1');
    let text22 = titles[2].querySelector('.text-2');

    tlTitles.set([numbers] ,{x:vw, y:-vh});
    tlTitles.set([text1, text2, text11, text22] ,{x:0, y:0});
    

      tlTitles.to(titles[0] ,   1,  {x:0, y:0, rotation:0.01                   , ease: Power0.easeNone},   0);
      tlTitles.to(titles[0] ,   2,  {y:-10, rotation:0.01                      , ease: Power0.easeNone},   1);
      tlTitles.to(titles[0] , 1.5,  {x:-vw*3, y:-100, rotation:0.01            , ease: Power0.easeNone},   3);
    
      tlTitles.fromTo(text2 , 0.8,  {x:60, y: 30},  {x:0, y:0                  , ease: Power1.easeOut}, 2.9);
      tlTitles.to(titles[1] , 0.8,  {x:0, y:0, rotation:0.01                   , ease: Power0.easeNone}, 2.5);
      tlTitles.to(titles[1] , 3.5,  {y:-10, rotation:0.01                      , ease: Power0.easeNone}, 3.5);
      tlTitles.to(titles[1] , 1.2,  {x:-vw*3, y:vh*2, rotation:0.01            , ease: Power0.easeNone},   7);
      tlTitles.to(text1     , 1.2,  {y:20, rotation:0.01                       , ease: Power0.easeNone},   7);
      tlTitles.to(text2     , 1.2,  {y:-20, rotation:0.01                      , ease: Power0.easeNone},   7);
    
      tlTitles.fromTo(text11 , 1.5, {x:20, y: 80},  {x:0, y:0                  , ease: Power1.easeInOut}, 6.3);
      tlTitles.to(titles[2] , 1,    {x:0, y:0, rotation:0.01                   , ease: Power0.easeNone},  6.3);
      tlTitles.to(titles[2] , 3.3,  {x:0, y:-10, rotation:0.01                 , ease: Power0.easeNone},  7.5); 
      tlTitles.to(titles[2] , 1.2,  {x:-vw*3, y:vh*2, rotation:0.01            , ease: Power0.easeNone}, 10.8);
      tlTitles.to(text11    , 1.2,  {y:20, rotation:0.01                       , ease: Power0.easeNone}, 10.8);
      tlTitles.to(text22    , 1.2,  {y:-20, rotation:0.01                      , ease: Power0.easeNone}, 10.8);
    
    
     tlBackgrounds.restart();
     tlPlayers.restart(); 
     tlLines.restart();
     tlNumbers.restart(); 
     tlTitles.restart(); 
    
    }
    