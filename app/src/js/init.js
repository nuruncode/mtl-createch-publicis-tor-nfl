let slideshow = document.querySelector('.hero-slideshow');
let slides = slideshow.querySelectorAll('.slide');

let numbers = slideshow.querySelectorAll('.number');
let titles = slideshow.querySelectorAll('.title');
let players = slideshow.querySelectorAll('.player');
let backgrounds = slideshow.querySelectorAll('.bgimg');
let verticalLine = slideshow.querySelector('.vertical-line');
let horizontalLine = slideshow.querySelector('.horizontal-line');
//Timelines
let tlBackgrounds;
let tlPlayers; 
let tlLines; 
let tlNumbers; 
let tlTitles; 

window.addEventListener("resize", function(){
  if(tlBackgrounds) { tlBackgrounds.kill(); }
  if(tlPlayers)     { tlPlayers.kill(); }
  if(tlLines)       { tlLines.kill(); }
  if(tlNumbers)     { tlNumbers.kill(); }
  if(tlTitles)      { tlTitles.kill(); }
  
animationSlides();
});
animationSlides();
