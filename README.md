# NFL 100 Challenge - Homepage Animation (2019)

## General
----

##### Description :

A hero banner for the NFL 100 challenge website

## Developers
----
##### Frontend :
- Ingrid Schwietzer


## Other informations
----
### Bitbucket :
https://bitbucket.org/nuruncode/mtl-createch-publicis-tor-nfl/


### Confluence :
https://wiki.nurun.com/display/MNRSWN


## Environnements
----
### DEV-LOCAL :
=> http://localhost:3000/


## Development Machine Setup
----
### 1. Clone the following repos :

```
mtl-createch-publicis-tor-nfl

```

### 6. Install npm modules:

From root folder install npm packages (nodejs must be install on the dev machine)
```
npm install
```


## Frontend
----
#### Folder Structure and build

UI files are in `src/` folder and get compiled - copied by gulp to `public/`.

#### Local server
```
gulp watch
```



## Version history
